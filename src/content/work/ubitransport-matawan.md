---
title: Matawan
publishDate: 2022-08-28 00:00:00
img: /assets/ubitransport-matawan.png
img_alt: Matawan services
description: |
  Participate in the development of a transportation management SAAS solution
tags:
  - Backend
  - API
  - Frontend
---

## In short

I worked on a cloud-centric backend used by various devices installed inside any transportation vehicle (bus, tram...) to give feedback to transport companies and allow passengers to travel between multiple transportation networks.

## Tasks

Design, create, maintain ApiPlatform REST API microservice  
Create a skeleton for microservices

*PHP 7.4 / 8, Symfony 4.4 / 5, PostgreSQL, Docker, PHPUnit*

Participate in the migration of a monolithic single-tenant
application to a multi-tenant microservice architecture

*CakePHP, PHP 5.6, PHP 7.4, RabbitMQ, Redis, Jenkins, GCP*

Create Vue.js components and maintain the related REST API to manage firmware versions of mobile and embedded hardware

*Vue.js, ApiPlatform*

Develop an interoperability system between different transportation networks using INTERBOB messages.

*CakePHP, PHP 7.4, FTP*

Integrate quality tools in GitLabCI and give a presentation
about them

*Infection, PHPStan*

Improve makefiles, docker images, Jenkins pipeline, code
coverage, and create helper scripts to help the team

*Make, Bash, Docker, Jenkins, Capistrano, Ruby, PHPUnit*

Write technical designs, cooperate fully in English with
Foreign developers
