---
title: Reservation system
publishDate: 2021-02-01 00:00:00
img: /assets/montessori-school-reservation-ui.png
img_alt: A calendar used to book cafeteria and childcare
description: |
  The school needed a reservation system for the school childcare and cafeteria
tags:
  - Backend
  - API
  - Authentication
---

## Project goal

The montessori school administrators asked me if I could help them keep track of the children that use the cafeteria and the childcare and export the data so they can generate exact bills.

Bonus: as a busy parent myself, I didn't want the users to register another account, so I had to find a way to have a secure access without asking the parents to register on the system.

## Solution

I've developed a custom API using PHP / Symfony hosted on a shared hosting and an administration panel with a strong password policy for the school administrators to see and export the data in CSV files. I've also added an interface in plain Javascript to the public website so the parents would be able to book slots for their kids.

Bonus:
Thanks to my custom authentication and migration system, the parents could use their existing account on an external website to use the reservation API by migrating their account to the reservation API on the first successful login.

From a developer perspective, I've used docker and quality tools to help me write good quality code.
