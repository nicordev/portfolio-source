---
title: ECNi
publishDate: 2023-08-02 00:00:00
img: /assets/ecni.png
img_alt: ECNi.fr
description: |
  We worked on the next version of the API of a social network for medical students
tags:
  - Backend
  - API
  - Frontend
---

## In short

As a consultant backend developer for <a href="https://www.ecni.fr/">ECNi</a>, I worked on the next version of the platform using DDD principles in a CQRS like architecture,using Behat to write our functional tests.

## Tasks

Implement commands, queries and policies following DDD principles with a CQRS like pattern

*PHP 8, Symfony 6, ApiPlatform 3*

Write and improve functional and unit tests

*Behat, PHPUnit*

Improve performance on some endpoints

*SQL, Doctrine, Postgresql*

Propose ideas to improve both the developer experience and the quality of the product

*Shortcut, notion*
