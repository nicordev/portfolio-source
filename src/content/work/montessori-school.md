---
title: Static website
publishDate: 2021-12-09 00:00:00
img: /assets/montessori-school-homepage.png
img_alt: Soft pink and baby blue water ripples together in a subtle texture.
description: |
  I've build a new website from scratch for an awesome school
tags:
  - Frontend
  - Static website
---

## Project goal

Initially, the school had a wordpress website that has been hacked due to a too easy to guess password.

The website was slow to load, most of the data was lost during the attack and the design was broken.

My tasks were to:
- Replace the existing hacked wordpress website by a more secure website.
- Create a new design
- Improve performance
- Lower the hosting platform costs

## Solution

I went for a static website using Jekyll and github pages for better security and performance.

I also moved the website form the shared hosting platform to github pages to lower the costs, so the school would have to pay for the domain name only.

Here is the result: [Montessori school website](http://montessori-violay.fr/)
